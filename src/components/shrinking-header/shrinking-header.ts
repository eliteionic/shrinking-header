import { Component, Input, ElementRef, Renderer, Output, EventEmitter, NgZone } from '@angular/core';

@Component({
	selector: 'shrinking-header',
	templateUrl: 'shrinking-header.html'
})
export class ShrinkingHeaderComponent {

	@Input('scrollArea') scrollArea: any;
	@Input('headerHeight') headerHeight: any;

	@Output() collapsed = new EventEmitter<boolean>();
	isCollapsed: boolean = false;

	constructor(public element: ElementRef, public renderer: Renderer, public zone: NgZone) {

	}

	ngAfterViewInit(){
		
		
		this.renderer.setElementStyle(this.element.nativeElement, 'height', this.headerHeight + 'px');

		this.scrollArea.ionScroll.subscribe((ev) => {
			this.resizeHeader(ev);
		});

	}

	resizeHeader(ev){

		let adjustedHeight = this.headerHeight - ev.scrollTop;

		if(adjustedHeight <= 0){

			adjustedHeight = 0;
			
			if(!this.isCollapsed){
				this.isCollapsed = true;
			
				this.zone.run(() => {
					this.collapsed.emit(true);
				});
				
			}

		}

		if(adjustedHeight > 0){

			if(this.isCollapsed){
				this.isCollapsed = false;
				this.zone.run(() => {
					this.collapsed.emit(false);
				});
			}
			
		}

		ev.domWrite(() => {
			this.renderer.setElementStyle(this.element.nativeElement, 'height', adjustedHeight + 'px');
		});

	}

}
